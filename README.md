# pch-session

Customized session with slightly different default settings and extensions. It includes an alternative Classic mode too.

Defaults are chosen following the ones I have been using for years and others requested by users of machines I maintain. It also relies on several extensions:
*	https://github.com/stuarthayhurst/alphabetical-grid-extension
*	https://github.com/ubuntu/gnome-shell-extension-appindicator
*	https://github.com/RaphaelRochet/applications-overview-tooltip
*	https://github.com/neffo/bing-wallpaper-gnome-extension
*	https://github.com/bjarosze/gnome-bluetooth-quick-connect
*	https://github.com/PRATAP-KUMAR/control-blur-effect-on-lock-screen
*	https://github.com/home-sweet-gnome/dash-to-panel
*	https://gitlab.com/rastersoft/desktop-icons-ng
*	https://github.com/GSConnect/gnome-shell-extension-gsconnect
*	https://github.com/JasonLG1979/gnome-shell-extension-weather-in-the-clock

Session PCH tries to have a mostly default Gnome Shell experience while PCH-Classic targets users that still feel more confortable with a "traditional" workflow
more similar to other OSes.

![ScreenShot](./pch.png)
![ScreenShot](./pch-classic.png)
